//#![feature(plugin)]
//#![plugin(protobuf_macros)]
#![feature(tlsv1_2)]

extern crate openssl;
extern crate protobuf;

use std::io::prelude::*;
use std::slice;
use std::path::Path;
use std::net::TcpStream;
use openssl::ssl::{Ssl, SslMethod, SslContext, SslStream, SSL_VERIFY_NONE};
use openssl::x509::{X509FileType};


use protobuf::Message;


mod Mumble;
use Mumble::{Version,Authenticate};


fn main() {
    let mut ctx = SslContext::new(SslMethod::Tlsv1_2).unwrap();

    let cert = Path::new("client.pem");
    let key = Path::new("client.key");

    ctx.set_cipher_list("DEFAULT");
    ctx.set_certificate_file(cert, X509FileType::PEM);
    ctx.set_private_key_file(key, X509FileType::PEM);


    ctx.set_verify(SSL_VERIFY_NONE, None);
    //Ok(Openssl { context: Arc::new(ctx) })

    let ssl = Ssl::new(&ctx).unwrap();
    let mut stream = TcpStream::connect("127.0.0.1:64738").unwrap();
    let mut sslStream = SslStream::connect(ssl, stream).unwrap();



    read_thread(sslStream);

    /*
    let version = protobuf_init!(Version::new(), {
            version: (0x1 << 16) | (0x2 << 8) | 0x4,
            release: "rust".to_string(),
            os:  "rust".to_string(),
            os_version: "9000".to_string()
    });
    */


    /*
    let mut buffer:Vec<u8>;

    match version.write_to_bytes() {
        Ok(data) => buffer = data,
        Err(err) => println!("Failed"),
    };
    */

    //let buffer = version.write_to_bytes().unwrap();


    //println!("testg {}", buffer[0]);

    //sslStream.write(buffer.as_slice())

    //sslStream.read(&mut buffer[..]);
}


fn read_thread<T: Read>(mut stream: T) {
    loop {
        let mut header_data = [0; 6];
        stream.read_exact(&mut header_data);
        let header = parse_header(header_data);
        let mut payload_data = Vec::with_capacity(header.packet_size as usize);
        stream.read_exact(&mut payload_data);
        //return;
    }
}


fn write_thread<T: Write>(stream: T){

}

#[derive(Debug)]
struct MumbleHeader {
    packet_type: u32,
    packet_size: u32
}

fn parse_header(header: [u8; 6]) -> MumbleHeader {
    let result = MumbleHeader{
        packet_type: (header[1] as u32) + ((header[0] as u32) << 8),
        packet_size: (header[5] as u32) + ((header[4] as u32) << 8) + ((header[3] as u32) << 16) + ((header[2] as u32) << 24)
    };

    println!("Header {:?}", result);
    return result;
}



/*
fn vec8_to_arr8(vector: Vec<u8>) -> [u8] {
    let mut result :  [u8; vector.size];
}
*/
